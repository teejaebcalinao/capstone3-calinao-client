import { useContext, useState } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import { useHistory } from 'react-router'
import { AppContext } from '../contexts/AppContext'

export default function LoginForm() {

    const { setUser } = useContext(AppContext)
    const [isLoading, setisLoading] = useState(false)
    const history = useHistory()
    const [credentials, setCredentials] = useState({
        email : "",
        password: ""
    })
    const handleChange = e =>{
        setCredentials({
            ...credentials,
            [e.target.id] : e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault()
        setisLoading(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/users/login`,{
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(credentials)
        })
        .then( res => res.json())
        .then( data =>{
/*            console.log(data)*/
            setisLoading(false)
            if (data.error) { 
                alert(data.error)
            } else {
                alert("Login successful")
                localStorage["token"] = data.accessToken
                fetch(`${process.env.REACT_APP_BE_URL}/api/users/`,{

                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }

                })
                .then(res => res.json())
                .then(data => {
                    /*console.log(data)*/
                    localStorage.setItem('email',data.email)
                    localStorage.setItem('isAdmin',data.isAdmin)

                    setUser(data)
                    history.push('/')
                })

            }
        }).catch( err=> console.log(err))
    }

    return (
        <Card>
            <Card.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="email">
                        <Form.Label>Email:</Form.Label>
                        <Form.Control type="email"  required onChange={handleChange} value={credentials.email} />
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Password:</Form.Label>
                        <Form.Control type="password"  required onChange={handleChange} value={credentials.password} />
                    </Form.Group>
                    {isLoading ? 
                        <Button type="submit" disabled>Login</Button>
                    : 
                        <Button type="submit">Login</Button>
                    }
                </Form>
            </Card.Body>
        </Card>
    )
}
