import { Button, Modal, Form, InputGroup, FormControl } from 'react-bootstrap'
import { useContext, useState } from 'react'
import { Pen, Trash } from 'react-bootstrap-icons'
import { AppContext } from '../contexts/AppContext';

export default function EntryListtem({entry}) {
/*    console.log(entry)*/
    const {categories,setLastEntryUpdate} = useContext(AppContext)
    const [isLoading, setisLoading] = useState(false)

    const [show, setShow] = useState(false);
    const [selectedEntry, setSelectedEntry] = useState(entry)
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleChange = e => {
        setSelectedEntry({...selectedEntry, [e.target.id]: e.target.value})
    }

    const handleSubmit = e =>{
        e.preventDefault()
        setisLoading(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/entries/${selectedEntry._id}`,{
            method: "PUT",
            headers: {
                "Authorization" :`Bearer ${localStorage["token"]}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(selectedEntry)
        })
        .then( res => res.json())
        .then( data => {
            setisLoading(false)
            if (!data.error) {
                alert("Record updated successfully")
                setLastEntryUpdate(data)
                handleClose()
            } else {
                alert("Check all fields")
            }
        })
        .catch(err => console.log(err))
    }
    
    const handleDelete = () =>{
        setisLoading(true)
        fetch(`${process.env.REACT_APP_BE_URL}/api/entries/${selectedEntry._id}`,{
            method: "DELETE",
            headers: {
                "Authorization" :`Bearer ${localStorage["token"]}`,
            }

        })
        .then( res => res.json())
        .then( data => {
            setisLoading(false)
            if (!data.error) {
                alert("Record deleted successfully")
                setLastEntryUpdate(data)
            }
        })
    }

    return (
        <tr>
            <td>{entry.category}</td>
            <td>&#8369; {entry.amount.toFixed(2)}</td>
            <td >
                <Button variant="outline-info" className="mx-1" onClick={handleShow}>
                    <Pen />
                </Button>
                {
                    isLoading ?
                    <Button variant="outline-danger" className="mx-1" disabled>
                        <Trash />
                    </Button> :
                    <Button variant="outline-danger" className="mx-1" onClick={handleDelete}>
                        <Trash />
                    </Button>
                }
                <Modal show={show} onHide={handleClose}>
                    <Form onSubmit={handleSubmit}>
                        <Modal.Header closeButton>
                        <Modal.Title>Update Category</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group controlId="category">
                                <Form.Label>Category</Form.Label>
                                <Form.Control as='select' value={selectedEntry.category} onChange={handleChange}>
                                    { categories.map( category => (
                                        selectedEntry.category === category._id ?
                                            <option key={category._id} value={category._id} selected>{category.name}</option>
                                        :
                                            <option key={category._id} value={category._id}>{category.name}</option>
                                        
                                    ))}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="type">
                                <Form.Label>Type</Form.Label>
                                <Form.Control as='select' value={selectedEntry.type}  onChange={handleChange}>
                                    <option value="income">Income</option>
                                    <option value="expense">Expense</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="amount">
                                <Form.Label>Amount</Form.Label>
                                <InputGroup className="mb-3">
                                    <InputGroup.Text>&#8369;</InputGroup.Text>
                                    <FormControl value={selectedEntry.amount}  onChange={handleChange}/>
                                    
                                </InputGroup>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            {isLoading ? 
                                <>
                                    <Button variant="secondary" disabled>
                                        Close
                                    </Button>
                                    <Button variant="primary" type="submit" disabled>
                                        Save Changes
                                    </Button>
                                </>
                            :
                                <>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Close
                                    </Button>
                                    <Button variant="primary" type="submit">
                                        Save Changes
                                    </Button>
                                </>
                            }
                        </Modal.Footer>
                    </Form>
                </Modal>
            </td>
        </tr>
    )
}
