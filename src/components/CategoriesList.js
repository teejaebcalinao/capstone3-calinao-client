import { useContext} from "react"
import { Col } from "react-bootstrap"
import CategoriesListItem from "./CategoriesListItem"
import { AppContext } from '../contexts/AppContext'

export default function CategoriesList() {

    const {categories} = useContext(AppContext)


    const displayCategories = categories.map( category =>(
        <Col className="my-1" xs={6} sm={5} md={4} lg={3} key={category._id}>
            <CategoriesListItem  category={category} />
        </Col>
    ))

    return (
        <>
            {displayCategories}
        </>
    )
}
