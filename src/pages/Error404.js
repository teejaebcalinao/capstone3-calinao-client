import React from 'react'

export default function Error404() {
    return (
        <div className="pt-5 mt-5 text-center">
            <h1>Error 404</h1>
            <p className="font-italic">Sorry! Page not found</p>
        </div>
    )
}
