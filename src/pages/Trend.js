import { useState, useEffect,useContext } from 'react'
import { Container } from 'react-bootstrap'
import { Line } from 'react-chartjs-2'
import moment from 'moment'
import { AppContext } from "../contexts/AppContext";
import {Redirect} from 'react-router-dom'

export default function Trend(){
    const {user,entries} = useContext(AppContext)
    /*console.log(entries)*/
    const [monthExpense,setMonthExpense] = useState([])
    const [monthIncome,setMonthIncome] = useState([])

/*    console.log(new Date())*/

/*    console.log(entries)*/
    
    useEffect(()=>{

        if( entries.length > 0 ){
        
        let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]
                entries.forEach( entry => {
                            const index = moment(entry.createdAt).month()//September = 8
                                if(entry.type === "expense" ){
                                    monthlyExpense[index] += entry.amount
                                }
                             /*console.log(monthlyExpense)*/  
                })
                setMonthExpense(monthlyExpense)

        let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
                entries.forEach( entry => {
                            const index = moment(entry.createdAt).month()//September = 8
                                if(entry.type === "income" ){
                                    monthlyIncome[index] += entry.amount
                                }
                             /*console.log(monthlyIncome) */ 
                })

                setMonthIncome(monthlyIncome)
        }

    },[entries])

/*    console.log(monthExpense)
    console.log(monthIncome)
*/
    const data = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',  'December'],
        datasets: [
            {
                label: 'Income',
                lineTension: 0.1,
                borderColor: 'blue',
                data: monthIncome,
            },
            {
                label: 'Expense',
                lineTension: 0.1,
                borderColor: 'red',
                data: monthExpense,
            }
        ],


    }
    const options = {
        legend: {
            display: false
        }
    }


    return (
        user._id
        ?
        <Container>
            <h3 className="my-5">Income vs Expense Trend</h3>
            <Line data={ data } options={ options }  height="100"/>

        </Container>
        
        :
        <Redirect to="/"/>
    )
}