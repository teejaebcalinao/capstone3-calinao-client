import { useContext } from 'react'
import { AppContext } from '../contexts/AppContext'
import { Col, Container, Row } from "react-bootstrap";
import EntryAddForm from "../components/EntryAddForm";
import EntryList from "../components/EntryList";
import {Redirect} from 'react-router-dom'

export default function Income() {
    const { user} = useContext(AppContext)

    return (
        user._id
        ?
        <Container className="my-5">
            <Row>
                <Col xs={12} sm={10} md={8} lg={6} className="mx-auto my-3">
                    <h1 className="text-center">Income</h1>
                </Col>
            </Row>
            <Row>
                <Col xs={12} sm={10} md={8} lg={5} className="mx-auto my-3">
                    <h2>Add Record</h2>
                    <EntryAddForm type="income"/>
                </Col>
                <Col xs={12} sm={10} md={8} lg={6} className="mx-auto">
                    <EntryList type="income" />
                </Col>
            </Row>
        </Container>
        :
        <Redirect to="/" />
    )
}
